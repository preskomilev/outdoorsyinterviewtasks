import { Component, Input, OnInit } from '@angular/core';
import { Location, Price, Rental, RentalsService, User } from './services/rentals.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MessageService, ConfirmationService, DialogService]
})
export class AppComponent implements OnInit {

  searchIds: any
  searchNear: any
  searchSort: any
  minPrice: any
  maxPrice: any
  searchLimit: any
  searchOffset: any

  inputForm = new FormGroup({
    user_id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    type: new FormControl(''),
    model: new FormControl(''),
    make: new FormControl(''),
    year: new FormControl(''),
    length: new FormControl(''),
    sleeps: new FormControl(''),
    price: new FormControl(''),
    city: new FormControl(''),
    state: new FormControl(''),
    zip: new FormControl(''),
    country: new FormControl(''),
    lat: new FormControl(''),
    lng: new FormControl(''),
    image: new FormControl('')
  });

  title = 'webapp';

  rentalsList: Rental[] = []
  rental: Rental = new Rental()
  rentalDialog = false;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private rentalsService: RentalsService
  ) { }

  ngOnInit(): void {
    this.setTableColumns();
    this.getData();
  }

  getData() {
    this.rentalsService.getRentals().then(result => {
      if (result.data !== null) {
        this.rentalsList = result.data
      }
    })
  }

  search() {
    var searchParams = {
      ids: this.searchIds,
      near: this.searchNear,
      sort: this.searchSort,
      price_max: this.maxPrice,
      price_min: this.minPrice,
      limit: this.searchLimit,
      offset: this.searchOffset
    }

    this.rentalsService.getRentals(searchParams).then(result => {
      if (result.data !== null) {
        this.rentalsList = result.data
      }
    })

  }

  openNew() {
    this.clearDialog()
    this.rentalDialog = true;
    this.rental = new Rental()
  }

  hideDialog() {
    this.rentalDialog = false;
  }

  saveRental(event: any) {
    if (event.id) {
      event.user.id = +this.inputForm.get("user_id")?.value
      event.name = this.inputForm.get("name")?.value
      event.description = this.inputForm.get("description")?.value
      event.type = this.inputForm.get("type")?.value
      event.model = this.inputForm.get("model")?.value
      event.year = +this.inputForm.get("year")?.value
      event.length = +this.inputForm.get("length")?.value
      event.sleeps = +this.inputForm.get("sleeps")?.value
      event.price.day = +this.inputForm.get("price")?.value
      event.location.city = this.inputForm.get("city")?.value
      event.location.state = this.inputForm.get("state")?.value
      event.location.zip = this.inputForm.get("zip")?.value
      event.location.country = this.inputForm.get("country")?.value
      event.location.lat = +this.inputForm.get("lat")?.value
      event.location.lng = +this.inputForm.get("lng")?.value
      event.primary_image_url = this.inputForm.get("image")?.value
      event.updated = new Date()

      this.rentalsService.updateRentals(event).then(result => {

        this.getData();
        this.rentalDialog = false
        this.showMessage(result)

      })

    } else {
      var rental: Rental = new Rental()
      var user: User = new User()
      var location: Location = new Location()
      var price: Price = new Price()

      user.id = +this.inputForm.get("user_id")?.value
      rental.user = user
      rental.name = this.inputForm.get("name")?.value
      rental.description = this.inputForm.get("description")?.value
      rental.type = this.inputForm.get("type")?.value
      rental.model = this.inputForm.get("model")?.value
      rental.make = this.inputForm.get("make")?.value
      rental.year = +this.inputForm.get("year")?.value
      rental.length = +this.inputForm.get("length")?.value
      rental.sleeps = +this.inputForm.get("sleeps")?.value
      price.day = +this.inputForm.get("price")?.value
      rental.price = price
      location.city = this.inputForm.get("city")?.value
      location.state = this.inputForm.get("state")?.value
      location.zip = this.inputForm.get("zip")?.value
      location.country = this.inputForm.get("country")?.value
      location.lat = +this.inputForm.get("lat")?.value
      location.lng = +this.inputForm.get("lng")?.value
      rental.location = location
      rental.primary_image_url = this.inputForm.get("image")?.value
      rental.updated = new Date()
      rental.created = new Date()

      this.rentalsService.createRentals(rental).then(result => {

        this.getData();
        this.rentalDialog = false
        this.showMessage(result)

      })

    }
  }

  editRental(rental: any) {
    this.inputForm.patchValue({
      user_id: rental.user.id,
      name: rental.name,
      description: rental.description,
      type: rental.type,
      make: rental.make,
      model: rental.model,
      year: rental.year,
      length: rental.length,
      sleeps: rental.sleeps,
      price: rental.price.day,
      city: rental.location.city,
      state: rental.location.state,
      zip: rental.location.zip,
      country: rental.location.country,
      lat: rental.location.lat,
      lng: rental.location.lng,
      image: rental.primary_image_url,
    })
    this.rentalDialog = true;
    this.rental = rental
  }

  deleteRental(rental: any) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + rental.name + ' ?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        var id = rental.id

        this.rentalsService.deleteRentals(id).then(result => {
          this.showMessage(result)
          this.getData()
        })

      }
    });
  }

  clear() {
    this.searchIds = ""
    this.searchNear = ""
    this.searchSort = ""
    this.minPrice = ""
    this.maxPrice = ""
    this.searchLimit = ""
    this.searchOffset = ""
  }

  clearDialog() {
    this.inputForm.patchValue({
      user_id: '',
      name: '',
      description: '',
      type: '',
      model: '',
      year: '',
      length: '',
      sleeps: '',
      price: '',
      city: '',
      state: '',
      zip: '',
      country: '',
      lat: '',
      lng: '',
      image: '',
    })
  }

  showMessage(response: any) {
    switch (response.status) {
      case 200:
        this.messageService.add({ severity: 'success', summary: 'Success', detail: response.messages[0][1] });
        break;
      case 403:
        this.messageService.add({ severity: 'error', summary: 'Warning', detail: response.messages[0][1] });
        break;
      default:
        this.messageService.add({ severity: 'info', summary: 'Warning', detail: response.messages[0][1] });
    }
  }

  setTableColumns() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' },
      { field: 'type', header: 'Type' },
      { field: 'make', header: 'Make' },
      { field: 'model', header: 'Model' },
      { field: 'year', header: 'Year' },
      { field: 'length', header: 'Length' },
      { field: 'sleeps', header: 'Sleeps' },
      { field: 'price', subfield: 'day', header: 'Price' },
      { field: 'location', subfield: 'city', header: 'City' },
      { field: 'location', subfield: 'state', header: 'State' },
      { field: 'location', subfield: 'zip', header: 'Zip' },
      { field: 'location', subfield: 'country', header: 'Country' },
      { field: 'location', subfield: 'lat', header: 'Latitude' },
      { field: 'location', subfield: 'lng', header: 'Longitude' },
      { field: 'user', subfield: 'first_name', header: 'First name' },
      { field: 'user', subfield: 'last_name', header: 'Last name' },
    ];

    this._selectedColumns = this.cols;
  }

  cols: any[] = [];
  _selectedColumns: any[] = [];
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }
  set selectedColumns(val: any[]) {
    this._selectedColumns = this.cols.filter(col => val.includes(col));
  }

}
