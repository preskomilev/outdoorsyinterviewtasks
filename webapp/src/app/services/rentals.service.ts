import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

export class Rental {
  id?: number;
  name?: string;
  description?: string;
  type?: string;
  make?: string;
  model?: string;
  year?: number;
  length?: number;
  sleeps?: number;
  primary_image_url?: string;
  created?: Date
  updated?: Date
  price?: Price;
  location?: Location;
  user?: User;

  public constructor(init?: Partial<Rental>) {
    Object.assign(this, init);
  }
}

export class Location {
  city?: string;
  state?: string;
  zip?: string;
  country?: string;
  lat?: number;
  lng?: number;

  public constructor(init?: Partial<Location>) {
    Object.assign(this, init);
  }
}

export class User {
  id?: number;
  first_name?: string;
  last_name?: string;

  public constructor(init?: Partial<User>) {
    Object.assign(this, init);
  }
}

export class Price {
  day?: number;

  public constructor(init?: Partial<Price>) {
    Object.assign(this, init);
  }
}

@Injectable({
  providedIn: 'root'
})
export class RentalsService {

  baseUrl: string = 'http://localhost:8242/rentals'
  readonly headers = new HttpHeaders().set('Content-type', 'application/json')

  constructor(
    private http: HttpClient,
  ) { }

  getRentals(searchParams?: any): Promise<any> {
    let params = new HttpParams();
    if (searchParams != undefined) {
      params = params.append('ids', searchParams.ids != undefined ? searchParams.ids : '');
      params = params.append('near', searchParams.near != undefined ? searchParams.near : '');
      params = params.append('sort', searchParams.sort != undefined ? searchParams.sort : '');
      params = params.append('price_min', searchParams.price_min != undefined ? searchParams.price_min : '');
      params = params.append('price_max', searchParams.price_max != undefined ? searchParams.price_max : '');
      params = params.append('limit', searchParams.limit != undefined ? searchParams.limit : '');
      params = params.append('offset', searchParams.offset != undefined ? searchParams.offset : '');
    }

    return this.http.get<any>(this.baseUrl, { params })
      .pipe(
        map(data => {
          return data;
        }),
        catchError(errorRes => {
          return throwError(errorRes);
        })
      ).toPromise();
  }

  updateRentals(rental: Rental) {
    return this.http.put<any>(this.baseUrl, rental)
      .pipe(
        map(data => {
          return data;
        }),
        catchError(errorRes => {
          return throwError(errorRes);
        })
      ).toPromise();
  }

  createRentals(rental: Rental) {
    return this.http.post<any>(this.baseUrl, rental)
      .pipe(
        map(data => {
          return data;
        }),
        catchError(errorRes => {
          return throwError(errorRes);
        })
      ).toPromise();
  }

  async deleteRentals(id: any) {
    return this.http.delete<any>(this.baseUrl + '/' + id)
      .pipe(
        map(data => {
          return data;
        }),
        catchError(errorRes => {
          return throwError(errorRes);
        })
      ).toPromise();
  }

}
