package controller

import (
	"database/sql"
	"fmt"
	u "server/apiHelpers"
	config "server/config"
	"strconv"
	"strings"

	"server/models"

	"github.com/gin-gonic/gin"
)

func GetSingleRental(c *gin.Context) {

	db := config.DbConfig()
	defer db.Close()
	var filters []interface{}

	id := c.Params.ByName("id")
	filters = append(filters, id)

	sql := `SELECT r.id, r.name, r.type, r.description,
				r.sleeps, r.price_per_day, r.home_city, r.home_state,
				r.home_zip, r.home_country, r.vehicle_make, r.vehicle_model,
				r.vehicle_year, r.vehicle_length, r.created, r.updated,
				r.lat, r.lng, r.primary_image_url, u.id,
				u.first_name, u.last_name
			FROM rentals as r
			join users as u on u.id = r.user_id
			where r.user_id = $1 `

	results, err := db.Query(sql, filters...)
	if err != nil {
		fmt.Print(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", "Summary:" + err.Error()}}))
		return
	}

	var item models.Rental
	var items []models.Rental

	for results.Next() {
		err = results.Scan(&item.ID, &item.Name, &item.Type, &item.Description,
			&item.Sleeps, &item.Price.Day, &item.Location.City, &item.Location.State,
			&item.Location.Zip, &item.Location.Country, &item.Make, &item.Model,
			&item.Year, &item.Length, &item.Created, &item.Updated,
			&item.Location.Lat, &item.Location.Lng, &item.PrimaryImageURL, &item.User.ID,
			&item.User.FirstName, &item.User.LastName)
		if err != nil {
			fmt.Print(err.Error())
			u.Respond(c.Writer, u.Message(403, [][]string{{"Error", "Summary:" + err.Error()}}))
			return
		}
		items = append(items, item)
	}

	response := u.Message(200, [][]string{{"Success", "Summary: " + strconv.Itoa(len(items)) + " rentals are returned"}})

	response["data"] = &items
	u.Respond(c.Writer, response)

}

func GetRentals(c *gin.Context) {

	db := config.DbConfig()
	defer db.Close()

	var where string
	var rowCount string
	var sortParam string
	var price_min int
	var price_max int
	var lat float64
	var lng float64
	idsList := []int{}

	price_min, _ = strconv.Atoi(c.Query("price_min"))
	price_max, _ = strconv.Atoi(c.Query("price_max"))
	if price_min != 0 && price_max != 0 {
		where = where + " and r.price_per_day BETWEEN " + strconv.Itoa(price_min) + " and " + strconv.Itoa(price_max) + " "
	}

	ids := c.Query("ids")
	if ids != "" {
		s := strings.Split(ids, ",")

		sSep := strings.Join(s, ",")

		for _, i := range s {
			j, err := strconv.Atoi(i)
			if err != nil {
				u.Respond(c.Writer, u.Message(403, [][]string{{"Error", "Summary:" + err.Error()}}))
			}
			idsList = append(idsList, j)
		}

		where = where + "  and r.id IN(" + sSep + ") "
	}

	near := c.Query("near")
	if near != "" {
		coordinates := strings.Split(near, ",")
		lat, _ = strconv.ParseFloat(coordinates[0], 64)
		lng, _ = strconv.ParseFloat(coordinates[1], 64)
		where = where + " and SQRT(POW(69.1 * (lat - " + fmt.Sprintf("%f", lat) + "), 2) + POW(69.1 * (" + fmt.Sprintf("%f", lng) + " - lng) * COS(lat / 57.3), 2)) < 100 "
	}

	sort := c.Query("sort")
	if sort != "" && sort == "id" {
		sortParam = " order by r.user_id asc "
	}
	if sort != "" && sort == "name" {
		sortParam = " order by r.name asc "
	}
	if sort != "" && sort == "type" {
		sortParam = " order by r.type asc "
	}
	if sort != "" && sort == "description" {
		sortParam = " order by r.description asc "
	}
	if sort != "" && sort == "sleeps" {
		sortParam = " order by r.sleeps asc "
	}
	if sort != "" && sort == "price" {
		sortParam = " order by r.price_per_day asc "
	}
	if sort != "" && sort == "city" {
		sortParam = " order by r.home_city asc "
	}
	if sort != "" && sort == "state" {
		sortParam = " order by r.home_state asc "
	}
	if sort != "" && sort == "zip" {
		sortParam = " order by r.home_zip asc "
	}
	if sort != "" && sort == "country" {
		sortParam = " order by r.home_country asc "
	}
	if sort != "" && sort == "make" {
		sortParam = " order by r.vehicle_make asc "
	}
	if sort != "" && sort == "model" {
		sortParam = " order by r.vehicle_model asc "
	}
	if sort != "" && sort == "year" {
		sortParam = " order by r.vehicle_year asc "
	}
	if sort != "" && sort == "created" {
		sortParam = " order by r.created asc "
	}
	if sort != "" && sort == "updated" {
		sortParam = " order by r.updated asc "
	}
	if sort != "" && sort == "lag" {
		sortParam = " order by r.lag asc "
	}
	if sort != "" && sort == "lng" {
		sortParam = " order by r.lng asc "
	}
	if sort != "" && sort == "firstname" {
		sortParam = " order by u.first_name asc "
	}
	if sort != "" && sort == "lastname" {
		sortParam = " order by u.last_name asc "
	}

	offset := c.Query("offset")
	limit := c.Query("limit")
	if (offset != "" && limit == "") || (offset == "" && limit != "") {
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", "Set both parameters: limit and offset"}}))
		return
	}
	if offset != "" && limit != "" {
		rowCount = fmt.Sprintf(" offset %v limit %v", offset, limit)
	}

	sql := `SELECT r.id, r.name, r.type, r.description,
				r.sleeps, r.price_per_day, r.home_city, r.home_state,
				r.home_zip, r.home_country, r.vehicle_make, r.vehicle_model,
				r.vehicle_year, r.vehicle_length, r.created, r.updated,
				r.lat, r.lng, r.primary_image_url, u.id,
				u.first_name, u.last_name
			FROM rentals as r
			join users as u on u.id = r.user_id
			where r.id is not null `
	if len(where) > 0 {
		sql = sql + where
	}
	if len(sortParam) > 0 {
		sql = sql + sortParam
	}
	if len(rowCount) > 0 {
		sql = sql + rowCount
	}

	results, err := db.Query(sql)
	if err != nil {
		fmt.Print(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", "Summary:" + err.Error()}}))
		return
	}

	var item models.Rental
	var items []models.Rental

	for results.Next() {
		err = results.Scan(&item.ID, &item.Name, &item.Type, &item.Description,
			&item.Sleeps, &item.Price.Day, &item.Location.City, &item.Location.State,
			&item.Location.Zip, &item.Location.Country, &item.Make, &item.Model,
			&item.Year, &item.Length, &item.Created, &item.Updated,
			&item.Location.Lat, &item.Location.Lng, &item.PrimaryImageURL, &item.User.ID,
			&item.User.FirstName, &item.User.LastName)
		if err != nil {
			fmt.Print(err.Error())
			u.Respond(c.Writer, u.Message(403, [][]string{{"Error", "Summary:" + err.Error()}}))
			return
		}
		items = append(items, item)
	}

	response := u.Message(200, [][]string{{"Success", "Summary: " + strconv.Itoa(len(items)) + " rentals are returned"}})

	response["data"] = &items
	u.Respond(c.Writer, response)

}

func CreateRentals(c *gin.Context) {

	db := config.DbConfig()
	defer db.Close()

	var item models.Rental

	err := c.BindJSON(&item)
	if err != nil {
		fmt.Println(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", err.Error()}}))
		return
	}

	sql := `insert into rentals(user_id, name, type, description,
				sleeps, price_per_day, home_city, home_state,
				home_zip, home_country, vehicle_make, vehicle_model,
				vehicle_year, vehicle_length, created, updated,
				lat, lng, primary_image_url)
			values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)`
	_, err = db.Exec(sql, item.User.ID, item.Name, item.Type, item.Description, item.Sleeps,
		item.Price.Day, item.Location.City, item.Location.State, item.Location.Zip,
		item.Location.Country, item.Make, item.Model, item.Year,
		item.Length, item.Created, item.Updated, item.Location.Lat,
		item.Location.Lng, item.PrimaryImageURL)

	if err != nil {
		fmt.Print(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", err.Error()}}))
		return
	}

	response := u.Message(200, [][]string{{"Success", "Summary: Data is inserted"}})
	response["data"] = &item
	u.Respond(c.Writer, response)

}

func UpdateRentals(c *gin.Context) {

	db := config.DbConfig()
	defer db.Close()

	var item models.Rental

	err := c.BindJSON(&item)
	if err != nil {
		fmt.Println(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", err.Error()}}))
		return
	}

	sql := ` UPDATE rentals SET user_id=$1, name=$2, type=$3, description=$4,
		sleeps=$5, price_per_day=$6, home_city=$7, home_state=$8,
		home_zip=$9, home_country=$10, vehicle_make=$11, vehicle_model=$12,
		vehicle_year=$13, vehicle_length=$14, created=$15, updated=$16,
		lat=$17, lng=$18, primary_image_url=$19 WHERE id=$20 `
	res, err := db.Exec(sql, item.User.ID, item.Name, item.Type, item.Description, item.Sleeps,
		item.Price.Day, item.Location.City, item.Location.State, item.Location.Zip,
		item.Location.Country, item.Make, item.Model, item.Year,
		item.Length, item.Created, item.Updated, item.Location.Lat,
		item.Location.Lng, item.PrimaryImageURL, item.ID)

	if err != nil {
		fmt.Print(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", err.Error()}}))
		return
	}

	rowsAffected, err := res.RowsAffected()

	if err != nil {
		fmt.Print(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", err.Error()}}))
		return
	}

	response := u.Message(200, [][]string{{"Success", "Summary: Total rows/record affected " + strconv.FormatInt(rowsAffected, 10) + ""}})
	response["data"] = &item
	u.Respond(c.Writer, response)

}

func DeleteRentals(c *gin.Context) {

	db := config.DbConfig()
	defer db.Close()

	id := c.Params.ByName("id")
	if id == "" {
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", "Summary: Enter id for delete"}}))
		return
	}

	var res sql.Result
	var err error

	sql := `DELETE from rentals where id = $1 `
	res, err = db.Exec(sql, id)
	if err != nil {
		fmt.Print(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", err.Error()}}))
		return
	}

	rowsAffected, err := res.RowsAffected()

	if err != nil {
		fmt.Print(err.Error())
		u.Respond(c.Writer, u.Message(403, [][]string{{"Error", err.Error()}}))
		return
	}

	response := u.Message(200, [][]string{{"Success", "Summary: Total rows/record affected " + strconv.FormatInt(rowsAffected, 10) + ""}})
	response["data"] = id
	u.Respond(c.Writer, response)
}
