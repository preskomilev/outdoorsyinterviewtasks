package controller

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestGetRentals(t *testing.T) {
	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	GetRentals(c)
	if w.Code == 200 {
		t.Logf("GetRentals List Success")
	}
}

func TestGetSingleRental(t *testing.T) {
	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Set("id", "0")
	GetSingleRental(c)
	if w.Code == 200 {
		t.Logf("GetSingleRental Success")
	} else if w.Code == 404 {
		t.Logf("Rental not found in DB")
	} else {
		t.Fail()
	}
}

func TestCreateRentals(t *testing.T) {
	gin.SetMode(gin.TestMode)

	jsonData, err := json.Marshal(map[string]interface{}{
		"user_id":           1,
		"name":              "2002 Mercedes E-class",
		"description":       "vehicle",
		"type":              "car",
		"make":              "Mercedes",
		"model":             "E-class",
		"year":              2002,
		"length":            2.3,
		"sleeps":            0,
		"primary_image_url": "https://www.auto-data.net/images/f23/Mercedes-Benz-E-class-W211.jpg",
		"created":           "2022-01-18T22:42:06.478595Z",
		"updated":           "2022-01-18T22:42:06.478595Z",
		"price": map[string]interface{}{
			"day": 5300,
		},
		"location": map[string]interface{}{
			"city":    "Varna",
			"state":   "Varna",
			"zip":     "9000",
			"country": "Bulgaria",
			"lat":     32.83,
			"lng":     -117.28,
		},
	})
	if err != nil {
		t.Fail()
	}
	req, err := http.NewRequest(http.MethodPost, "/rentals", bytes.NewBuffer(jsonData))
	if err != nil {
		t.Fail()
	}
	req.Header.Set("Content-type", "application/json")
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = req
	CreateRentals(c)
	bodySb, err := ioutil.ReadAll(w.Body)
	if err != nil {
		t.Fatalf("Error reading body: %v\n", err)
	}
	body := string(bodySb)
	t.Logf("Body: %v\n", body)
	var decodedResponse interface{}
	err = json.Unmarshal(bodySb, &decodedResponse)
	if err != nil {
		t.Fatalf("Cannot decode response <%p> from server. Err: %v", bodySb, err)
	}
	if w.Code == 201 {
		t.Logf("CreateRentals Success")
	} else if w.Code == 409 {
		t.Logf("Prevent duplicated rental creation success")
	} else if w.Code == 422 {
		t.Logf("Prevent duplicated rental creation success")
	} else {
		t.Fail()
	}
}
