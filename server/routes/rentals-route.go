package routes

import (
	"os"
	controller "server/controllers"

	"github.com/gin-gonic/gin"
)

type Routes struct {
}

func (c Routes) StartGin(port string) {
	r := gin.Default()

	r.Use(func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	})

	api := r.Group(os.Getenv("API_URL"))
	{
		api.GET("rentals/:id", controller.GetSingleRental)
		api.GET("rentals", controller.GetRentals)
		api.POST("rentals", controller.CreateRentals)
		api.PUT("rentals", controller.UpdateRentals)
		api.DELETE("rentals/:id", controller.DeleteRentals)
	}
	r.Run(":" + port)
}
