package models

import "time"

type Rental struct {
	ID              int       `json:"id"`
	Name            string    `json:"name"`
	Description     string    `json:"description"`
	Type            string    `json:"type"`
	Make            string    `json:"make"`
	Model           string    `json:"model"`
	Year            int       `json:"year"`
	Length          float64   `json:"length"`
	Sleeps          int       `json:"sleeps"`
	PrimaryImageURL string    `json:"primary_image_url"`
	Created         time.Time `json:"created"`
	Updated         time.Time `json:"updated"`
	Price           struct {
		Day int `json:"day"`
	} `json:"price"`
	Location struct {
		City    string  `json:"city"`
		State   string  `json:"state"`
		Zip     string  `json:"zip"`
		Country string  `json:"country"`
		Lat     float64 `json:"lat"`
		Lng     float64 `json:"lng"`
	} `json:"location"`
	User struct {
		ID        int    `json:"id"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
	} `json:"user"`
}
