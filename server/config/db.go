package config

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/lib/pq"
)

func Connect() (*sql.DB, error) {

	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")
	dbPort := os.Getenv("db_port")

	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, username, password, dbName)
	conn, err := sql.Open("postgres", connStr)

	if err != nil {
		return nil, err
	}

	return conn, nil

}

func DbConfig() *sql.DB {
	db, err := Connect()
	if err != nil {
		fmt.Printf("\nError while connecting to database!\n")
		panic(err)
	}
	fmt.Printf("\nSuccessfully connected to database!\n")
	return db
}
