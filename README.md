# Web application written on Go and Angular #

The server side is written on Golang and represents a rentals JSON API service with CRUD operations. The front end is written on Angular and represents server operations.

### How do I get set up? ###

* Running docker-compose up will automatically generate a postgres database and server api
* To run the front end, you have to clone the repository and run angular app manually after that you have to build the application:

    ```
   cd webapp
   npm install
   ng serve
   ```
This will start application by default on localhost:4200