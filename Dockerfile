# FROM node:12.7-alpine AS ANGULAR_BUILD
# RUN npm install -g @angular/cli@12.2.8
# COPY webapp /webapp
# WORKDIR webapp
# RUN npm install && ng build --prod

FROM golang:1.17.1-alpine AS GO_BUILD
COPY server /server
COPY server/.env /server
WORKDIR /server
COPY . /server
RUN go build -o /go/bin/server

FROM alpine:3.10
WORKDIR app
# COPY --from=ANGULAR_BUILD /webapp/dist/webapp/* ./webapp/dist/webapp/
COPY --from=GO_BUILD /go/bin/server ./
COPY server/.env ./
RUN ls
CMD ./server